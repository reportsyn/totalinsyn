﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Threading;
using System.IO;
using System.Transactions;
using System.Diagnostics;

namespace SafeSynTools
{
    public partial class Form1 : Form
    {
        int in_count = Convert.ToInt32(ConfigurationManager.AppSettings["in_count"]);//获取实时同步的筆數
        int in_time = Convert.ToInt32(ConfigurationManager.AppSettings["in_time"]);//获取实时同步的执行间隔时间
        int in_waitX = Convert.ToInt32(ConfigurationManager.AppSettings["in_waitX"]);//获取实时同步的执行间隔倍數
        int win_count = Convert.ToInt32(ConfigurationManager.AppSettings["win_count"]);//获取实时同步的筆數
        int win_time = Convert.ToInt32(ConfigurationManager.AppSettings["win_time"]);//获取实时同步的执行间隔时间
        int win_waitX = Convert.ToInt32(ConfigurationManager.AppSettings["win_waitX"]);//获取实时同步的执行间隔倍數
        int points_count = Convert.ToInt32(ConfigurationManager.AppSettings["points_count"]);//获取实时同步的筆數
        int points_time = Convert.ToInt32(ConfigurationManager.AppSettings["points_time"]);//获取实时同步的执行间隔时间
        int points_waitX = Convert.ToInt32(ConfigurationManager.AppSettings["points_waitX"]);//获取实时同步的执行间隔倍數
        string errorLogPath = System.Environment.CurrentDirectory + "\\errorlog.txt";//错误日志记录路径
        string successLogPath = System.Environment.CurrentDirectory + "\\successlog.txt";//成功日志记录路径
        System.Timers.Timer timer_clear = new System.Timers.Timer();

        public Form1()
        {
            InitializeComponent();
            in_time = in_time * 1000;//秒
            win_time = win_time * 1000;//秒
            points_time = points_time * 1000;//秒
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            //创建实时同步的线程
            //Thread thread1 = new Thread(new ThreadStart(syn_dt_lottery_winning_record));
            //thread1.Start();

            Thread thread2 = new Thread(new ThreadStart(syn_dt_user_get_point_record));
            thread2.Start();

            //Thread thread3 = new Thread(new ThreadStart(syn_dt_user_in_account));
            //thread3.Start();

            timer_clear.AutoReset = false;
            timer_clear.Interval = 10000;
            timer_clear.Elapsed += new System.Timers.ElapsedEventHandler(clear_Elapse);
            timer_clear.Start();
        }

        #region 实时同步的方法
        void syn_dt_user_in_account()
        {
            string tablename = ConfigurationManager.AppSettings["onlyOne_in"].ToString();
            FillMsg1("正在同步...");
            int count = 0;
            int sleep_time = in_time;
            while (true)
            {
                count = 0;
                try
                {
                    List<SqlParameter> LocalSqlParamter = new List<SqlParameter>()
                    {
                        new SqlParameter("@tableName",tablename)
                    };
                    string querystr = "select lockid from dt_tablelockid where tableName=@tableName";
                    var locktable = SqlDbHelper.GetQueryFromLocalData(querystr, LocalSqlParamter.ToArray());
                    byte[] lockid = StringConvertByte(locktable.Rows[0]["lockid"].ToString());
                    List<string> maxLockid_list = new List<string>();
                    List<SqlParameter> AzureSqlParamter = new List<SqlParameter>();
                    SqlParameter pa_lockid = new SqlParameter("@lockid", SqlDbType.Timestamp);
                    pa_lockid.Value = lockid;
                    AzureSqlParamter.Add(pa_lockid);
                    string str = "select top " + in_count  + " * from " + tablename + " where lockid>@lockid and user_id in(select id from dt_users where flog=0) order by lockid  asc";
                    var table = SqlDbHelper.GetQueryFromAzure(str, AzureSqlParamter.ToArray());
                    count = table.Rows.Count;
                    if (table.Rows.Count != 0)//有数据时更新到本地数据库中
                    {
                        for (int i = table.Rows.Count - 1; i >= 0; i--)
                        {
                            byte[] a = (byte[])table.Rows[i]["lockid"];
                            string lockid_list = BitConverter.ToString(a).Replace("-", "");
                            maxLockid_list.Add(lockid_list);
                        }
                        string maxlockid = "0x" + maxLockid_list.Max();
                        table.Columns.Remove("lockid");
                        TimeSpan ts1 = new TimeSpan(DateTime.Now.Ticks);
                        int result = SqlDbHelper.RunInsert(table, maxlockid, "dsp_user_in_account_synSum");
                        if (result == 1)
                        {
                            FillErrorMsg(tablename + "执行存储过程失败" + DateTime.Now.ToString());
                            WriteErrorLog(tablename + ":" + DateTime.Now.ToString(), "执行存储过程失败");
                        }
                        else
                        {
                            TimeSpan ts2 = new TimeSpan(DateTime.Now.Ticks);
                            TimeSpan ts = ts1.Subtract(ts2).Duration();
                            string dateDiff = ts.Seconds.ToString() + "秒";
                            FillMsg1(tablename + "成功同步汇总" + count + "条数据,耗时:" + dateDiff);
                            WriteLogData(tablename + "成功同步汇总" + count + "条数据,耗时:" + dateDiff + "     同步汇总时间:" + DateTime.Now.ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (ex.Message != "未将对象引用设置到对象的实例。")
                    {
                        FillErrorMsg(tablename + ":" + ex);
                        WriteErrorLog(tablename + ":" + DateTime.Now.ToString(), ex.ToString());
                    }
                }
                if (count == in_count)
                {
                    sleep_time = 2000;
                }
                else if (count >= in_count / 3 * 2)
                {
                    sleep_time = in_time;
                }
                else if (count < in_count / 3 * 2 && count >= in_count / 3)
                {
                    sleep_time = in_time * in_waitX;
                }
                else if (count < in_count / 3)
                {
                    sleep_time = in_time * (in_waitX + 2);
                }
                Thread.Sleep(sleep_time);//睡眠时间
            }
        }

        void syn_dt_lottery_winning_record()
        {
            string tablename = ConfigurationManager.AppSettings["onlyOne_win"].ToString();
            FillMsg2("正在同步...");
            int count = 0;
            int sleep_time = win_time;
            while (true)
            {
                if (DateTime.Now.DayOfWeek == DayOfWeek.Friday && DateTime.Now.Hour >= 3 && DateTime.Now.Hour < 6)
                {
                    Thread.Sleep(1000 * 60 * 60 * 3);
                    continue;
                }
                count = 0;
                try
                {
                    List<SqlParameter> LocalSqlParamter = new List<SqlParameter>()
                            {
                                new SqlParameter("@tableName",tablename)
                            };
                    string querystr = "select lockid from dt_tablelockid where tableName=@tableName";
                    var locktable = SqlDbHelper.GetQueryFromLocalData(querystr, LocalSqlParamter.ToArray());
                    byte[] lockid = StringConvertByte(locktable.Rows[0]["lockid"].ToString());
                    List<SqlParameter> AzureSqlParamter = new List<SqlParameter>();
                    SqlParameter pa_lockid = new SqlParameter("@lockid", SqlDbType.Timestamp);
                    pa_lockid.Value = lockid;
                    AzureSqlParamter.Add(pa_lockid);
                    List<string> maxLockid_list = new List<string>();
                    string str = "select top " + win_count + " *  from " + tablename + " where lockid>@lockid and  flog=0 order by lockid asc";
                    var table = SqlDbHelper.GetQueryFromAzure(str, AzureSqlParamter.ToArray());
                    count = table.Rows.Count;
                    if (table.Rows.Count != 0)//有数据时更新到本地数据库中
                    {
                        for (int i = table.Rows.Count - 1; i >= 0; i--)
                        {
                            byte[] a = (byte[])table.Rows[i]["lockid"];
                            string lockid_list = BitConverter.ToString(a).Replace("-", "");
                            maxLockid_list.Add(lockid_list);
                        }

                        ////檢查本地有沒有一樣的id
                        //string sql_in_str = "";
                        //for (int r = 0; r < table.Rows.Count; r++)
                        //{
                        //    sql_in_str += table.Rows[r]["id"].ToString().TrimEnd() + ",";
                        //}
                        //sql_in_str = sql_in_str.Substring(0, sql_in_str.Length - 1);
                        //string str_local = string.Format("select id from {0} where id in (" + sql_in_str + ")", "dt_lottery_winning_verify");
                        //DataTable dt_select = SqlDbHelper.GetQueryFromLocalData(str_local);
                        ////重複則刪除
                        //for (int r = 0; r < dt_select.Rows.Count; r++)
                        //{
                        //    DataRow [] foundRows = table.Select("id = '" + dt_select.Rows[r]["id"] + "'");
                        //    table.Rows.Remove(foundRows[0]);
                        //}
                        //count = table.Rows.Count;
                        ////沒有重複id的做group by
                        //string id_str ="";
                        //for(int i=0; i < table.Rows.Count; i++)
                        //{
                        //    id_str += table.Rows[i]["id"].ToString().TrimEnd() + ",";
                        //}
                        //if (id_str.Length > 0)
                        //    id_str = id_str.Substring(0, id_str.Length - 1);
                        //else
                        //    id_str = "0";
                        //string strGroup = "select row_number() over(order by identityid) as rownumber,identityid, user_id,lottery_code, sum(bonus_money) as money,sourcename,CONVERT(varchar(100), add_time, 23) as add_time,count(1) as count from " + tablename + " where id in(" + id_str + ") and lockid>@lockid and  flog=0 group by identityid,user_id,lottery_code,sourcename,CONVERT(varchar(100), add_time, 23) order by identityid,user_id,lottery_code,sourcename,CONVERT(varchar(100), add_time, 23)";
                        //var tableGroup = SqlDbHelper.GetQueryFromAzure(strGroup, AzureSqlParamter.ToArray());

                        string maxlockid = "0x" + maxLockid_list.Max();
                        table.Columns.Remove("lockid");
                        Stopwatch st = new Stopwatch();
                        st.Reset();
                        st.Start();
                        int result = SqlDbHelper.RunInsert(table, maxlockid, "dsp_lottery_winning_synSum_self");
                        if (result == 1)
                        {
                            FillErrorMsg(tablename + "执行存储过程失败" + DateTime.Now.ToString());
                            WriteErrorLog(tablename + ":" + DateTime.Now.ToString(), "执行存储过程失败");
                            st.Stop();
                        }
                        else
                        {
                            st.Stop();
                            string dateDiff = Math.Ceiling(Convert.ToDouble(st.ElapsedMilliseconds / 1000)).ToString() + "秒";
                            FillMsg2(tablename + "成功同步汇总" + count + "条数据,耗时:" + dateDiff);
                            WriteLogData(tablename + "成功同步汇总" + count + "条数据,耗时:" + dateDiff + "     同步汇总时间:" + DateTime.Now.ToString());
                            //FillMsg2(tablename + "原筆數" + count + "Group" + tableGroup.Rows.Count + ",耗时:" + dateDiff);
                            //FillMsg2("              group平均每筆耗時:" + Math.Round(Convert.ToDouble(st.ElapsedMilliseconds) / tableGroup.Rows.Count, 4) + "毫秒");
                            //WriteLogData(tablename + "原筆數" + count + "Group" + tableGroup.Rows.Count + ",耗时:" + dateDiff + "     同步汇总时间:" + DateTime.Now.ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (ex.Message != "未将对象引用设置到对象的实例。")
                    {
                        FillErrorMsg(tablename + ":" + ex);
                        WriteErrorLog(tablename + ":" + DateTime.Now.ToString(), ex.ToString());
                    }
                }
                if (count == win_count)
                {
                    sleep_time = 2000;
                }
                else if (count >= win_count / 3 * 2)
                {
                    sleep_time = win_time;
                }
                else if (count < win_count / 3 * 2 && count >= win_count / 3)
                {
                    sleep_time = win_time * win_waitX;
                }
                else if (count < win_count / 3)
                {
                    sleep_time = win_time * (win_waitX + 2);
                }
                Thread.Sleep(sleep_time);//睡眠时间
            }
        }

        void syn_dt_user_get_point_record()
        {
            string tablename = ConfigurationManager.AppSettings["onlyOne_point"].ToString();
            FillMsg3("正在同步...");
            int count = 0;
            int sleep_time = points_time;
            bool restart = false;
            while (true)
            {
                if (DateTime.Now.DayOfWeek == DayOfWeek.Friday && DateTime.Now.Hour >= 3 && DateTime.Now.Hour < 6)
                {
                    if (DateTime.Now.Hour == 3 && DateTime.Now.Minute >= 10 && restart == false)
                    {
                        try
                        {
                            Process.Start("cmd.exe", @"/c net stop SQLSERVERAGENT  & net stop MSSQLSERVER");
                            Thread.Sleep(30000);
                            Process.Start("cmd.exe", @"/c net start MSSQLSERVER  & net start SQLSERVERAGENT");
                            restart = true;
                        }
                        catch (Exception ex)
                        {
                            FillErrorMsg("重啟報錯:" + ex);
                            WriteErrorLog("重啟報錯:" + DateTime.Now.ToString(), ex.ToString());
                        }
                    }
                    else if (restart == false)
                    {
                        Thread.Sleep(1000 * 60 * 10);
                    }
                    else if (restart == true)
                    {
                        Thread.Sleep(1000 * 60 * 60 * 3 - 1000 * 60 * 9);
                    }
                    continue;
                }
                restart = false;
                count = 0;
                try
                {
                    List<SqlParameter> LocalSqlParamter = new List<SqlParameter>()
                            {
                                new SqlParameter("@tableName",tablename)
                            };
                    string querystr = "select lockid from dt_tablelockid where tableName=@tableName";
                    var locktable = SqlDbHelper.GetQueryFromLocalData(querystr, LocalSqlParamter.ToArray());
                    byte[] lockid = StringConvertByte(locktable.Rows[0]["lockid"].ToString());
                    List<SqlParameter> AzureSqlParamter = new List<SqlParameter>();
                    SqlParameter pa_lockid = new SqlParameter("@lockid", SqlDbType.Timestamp);
                    pa_lockid.Value = lockid;
                    AzureSqlParamter.Add(pa_lockid);

                    List<string> maxLockid_list = new List<string>();
                    string str = "select top " + points_count  + " *  from " + tablename + " where lockid>@lockid and user_id in(select id from dt_users where flog=0)  order by lockid asc";
                    var table = SqlDbHelper.GetQueryFromAzure(str, AzureSqlParamter.ToArray());
                    count = table.Rows.Count;
                    if (table.Rows.Count != 0)//有数据时更新到本地数据库中
                    {
                        for (int i = table.Rows.Count - 1; i >= 0; i--)
                        {
                            byte[] a = (byte[])table.Rows[i]["lockid"];
                            string lockid_list = BitConverter.ToString(a).Replace("-", "");
                            maxLockid_list.Add(lockid_list);
                        }
                        string maxlockid = "0x" + maxLockid_list.Max();
                        table.Columns.Remove("lockid");

                        TimeSpan ts1 = new TimeSpan(DateTime.Now.Ticks);
                        int result = SqlDbHelper.RunInsert(table, maxlockid, "dsp_user_get_point_synSum");
                        TimeSpan ts2 = new TimeSpan(DateTime.Now.Ticks);
                        TimeSpan ts = ts1.Subtract(ts2).Duration();
                        string dateDiff = ts.Seconds.ToString() + "秒";
                        FillMsg3(tablename + "成功同步汇总" + count + "条数据,耗时:" + dateDiff);
                        WriteLogData(tablename + "成功同步汇总" + count + "条数据,耗时:" + dateDiff + "     同步汇总时间:" + DateTime.Now.ToString());
                    }
                }
                catch (Exception ex)
                {
                    if (ex.Message != "未将对象引用设置到对象的实例。")
                    {
                        FillErrorMsg(tablename + ":" + ex);
                        WriteErrorLog(tablename + ":" + DateTime.Now.ToString(), ex.ToString());
                    }
                }
                if (count == points_count)
                {
                    sleep_time = 2000;
                }
                else if (count >= points_count / 3 * 2)
                {
                    sleep_time = points_time;
                }
                else if (count < points_count / 3 * 2 && count >= points_count / 3)
                {
                    sleep_time = points_time * points_waitX;
                }
                else if (count < points_count / 3)
                {
                    sleep_time = points_time * (points_waitX + 2);
                }
                Thread.Sleep(sleep_time);//睡眠时间
            }
        }

        private byte[] StringConvertByte(string sqlstring)
        {
            string stringFromSQL = sqlstring;
            List<byte> byteList = new List<byte>();

            string hexPart = stringFromSQL.Substring(2);
            for (int i = 0; i < hexPart.Length / 2; i++)
            {
                string hexNumber = hexPart.Substring(i * 2, 2);
                byteList.Add((byte)Convert.ToInt32(hexNumber, 16));
            }

            byte[] original = byteList.ToArray();
            return original;
        }
        #endregion

        #region richTextBox记录
        private delegate void RichBox(string msg);
        private void FillMsg1(string msg) 
        {
            if (richTextBox1.InvokeRequired)
            {
                RichBox rb = new RichBox(FillMsg1);
                richTextBox1.Invoke(rb, new object[] { msg });
            }
            else 
            {
                if (richTextBox1.IsHandleCreated)
                {
                    richTextBox1.AppendText(msg);
                    richTextBox1.AppendText("\r\n");
                    richTextBox1.SelectionStart = richTextBox1.Text.Length;
                    richTextBox1.SelectionLength = 0;
                    richTextBox1.Focus();
                }
            }
        }

        private delegate void RichBox2(string msg);
        private void FillMsg2(string msg)
        {
            if (richTextBox2.InvokeRequired)
            {
                RichBox2 rb = new RichBox2(FillMsg2);
                richTextBox2.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (richTextBox2.IsHandleCreated)
                {
                    richTextBox2.AppendText(msg);
                    richTextBox2.AppendText("\r\n");
                    richTextBox2.SelectionStart = richTextBox2.Text.Length;
                    richTextBox2.SelectionLength = 0;
                    richTextBox2.Focus();
                }
            }
        }

        private delegate void RichBox3(string msg);
        private void FillMsg3(string msg)
        {
            if (richTextBox3.InvokeRequired)
            {
                RichBox3 rb = new RichBox3(FillMsg3);
                richTextBox3.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (richTextBox3.IsHandleCreated)
                {
                    richTextBox3.AppendText(msg);
                    richTextBox3.AppendText("\r\n");
                    richTextBox3.SelectionStart = richTextBox3.Text.Length;
                    richTextBox3.SelectionLength = 0;
                    richTextBox3.Focus();
                }
            }
        }

        private delegate void RichBoxErr(string msg);
        private void FillErrorMsg(string msg)
        {
            if (errorBox.InvokeRequired)
            {
                RichBoxErr rb = new RichBoxErr(FillErrorMsg);
                errorBox.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (errorBox.IsHandleCreated)
                {
                    errorBox.AppendText(msg);
                    errorBox.AppendText("\r\n");
                    errorBox.SelectionStart = errorBox.Text.Length;
                    errorBox.SelectionLength = 0;
                    errorBox.Focus();
                }
            }
        }
        #endregion

        #region 打印成功日志记录
        private object obj1 = new object();
        public void WriteLogData(string msgex)
        {
            lock (obj1)
            {
                if (!File.Exists(successLogPath))
                {
                    FileStream fs1 = new FileStream(successLogPath, FileMode.Create, FileAccess.Write);//创建写入文件 
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.Write(msgex);
                    sw.WriteLine();
                    sw.Close();
                    fs1.Close();
                }
                else
                {
                    FileStream fs = new FileStream(successLogPath, FileMode.Append, FileAccess.Write);
                    StreamWriter sr = new StreamWriter(fs);
                    sr.Write(msgex);
                    sr.WriteLine();
                    sr.Close();
                    fs.Close();
                }
            }
        }
        #endregion

        #region 错误日志记录
        private object obj = new object();
        public void WriteErrorLog(string msgex, string msgsql)
        {
            lock (obj)
            {
                if (!File.Exists(errorLogPath))
                {
                    FileStream fs1 = new FileStream(errorLogPath, FileMode.Create, FileAccess.Write);//创建写入文件 
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.WriteLine(msgex);
                    sw.WriteLine(msgsql);
                    sw.WriteLine();
                    sw.WriteLine();
                    sw.Close();
                    fs1.Close();
                }
                else
                {
                    FileStream fs = new FileStream(errorLogPath, FileMode.Append, FileAccess.Write);
                    StreamWriter sr = new StreamWriter(fs);
                    sr.WriteLine(msgex);
                    sr.WriteLine(msgsql);
                    sr.WriteLine();
                    sr.WriteLine();
                    sr.Close();
                    fs.Close();
                }
            }
        }
        #endregion

        #region 清理textbox
        private void clear_Elapse(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (System.DateTime.Now.ToString("mm") == "00")
                    ClearMsg();
            }
            catch (ThreadAbortException ex) { }
            catch (Exception ex2)
            {

            }
            finally
            {
                timer_clear.Start();
            }
        }
        private delegate void RichBoxClear();
        private void ClearMsg()
        {
            if (richTextBox1.InvokeRequired & richTextBox2.InvokeRequired & richTextBox3.InvokeRequired & errorBox.InvokeRequired)
            {
                RichBoxClear rb = new RichBoxClear(ClearMsg);
                richTextBox1.Invoke(rb);
                richTextBox2.Invoke(rb);
                richTextBox3.Invoke(rb);
                errorBox.Invoke(rb);
            }
            else
            {
                if (richTextBox1.IsHandleCreated)
                {
                    richTextBox1.Clear();
                    richTextBox2.Clear();
                    richTextBox3.Clear();
                    errorBox.Clear();
                }
            }
        }
        #endregion

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {

            Dispose();
            Application.Exit();
            System.Environment.Exit(0);
        }

    }
}
